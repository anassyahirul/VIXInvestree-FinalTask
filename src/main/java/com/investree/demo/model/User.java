package com.investree.demo.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class User {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "is_active")
    private boolean isActive;

    @OneToOne(mappedBy = "user")
    private UserDetail userDetail;

    @OneToMany
    private List<Transaksi> transaksiList;
}