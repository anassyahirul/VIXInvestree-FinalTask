package com.investree.demo.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class PaymentHistory {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "payment_history")
    @JoinColumn(name = "id_transaksi", referencedColumnName = "id")
    private List<Transaksi> transaksi;

    @Column(name = "pembayaran_ke")
    private int pembayaranKe;

    @Column(name = "jumlah")
    private Double jumlah;

    @Column(name = "bukti_pembayaran")
    private String buktiPembayaran;
}