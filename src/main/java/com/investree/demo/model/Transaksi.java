package com.investree.demo.model;

import javax.persistence.*;

@Entity
public class Transaksi {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // FK
    @ManyToOne
    @JoinColumn(name = "id_peminjam", referencedColumnName = "id")
    private User peminjam;

    // FK
    @ManyToOne
    @JoinColumn(name = "id_meminjam", referencedColumnName = "id")
    private User meminjam;

    @Column(name = "tenor")
    private int tenor;

    @Column(name = "total_pinjaman")
    private Double totalPinjaman;

    @Column(name = "bunga_persen")
    private Double bungaPersen;

    @Column(name ="status")
    private String status;

    @ManyToOne
    private PaymentHistory paymentHistory;
}
