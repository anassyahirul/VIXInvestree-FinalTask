package com.investree.demo.model;

import javax.persistence.*;

@Entity
public class UserDetail {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // FK
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_user", referencedColumnName = "id")
    private User user;

    @Column(name = "nama")
    private String nama;

    @Column(name = "alamat")
    private String alamat;
}
